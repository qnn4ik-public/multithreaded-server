#include <bits/stdc++.h>

#ifndef INC_THREADPOOL_H
#define INC_THREADPOOL_H


#define MAX_THREADS 10

typedef void* threadpool;

typedef void (* dispatch_fn)(void*);

threadpool create_threadpool(int num_threads_in_pool);

void dispatch(threadpool from_me,dispatch_fn dispatch_to_here,void* arg);

void destroy_threadpool(threadpool destroyme);

#endif